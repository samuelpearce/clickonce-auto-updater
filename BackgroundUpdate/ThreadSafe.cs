// http://www.codeproject.com/KB/dialog/GUI_update_from_thread.aspx
// added this.SetProgressBar as there was nothing to change value from another thread
// Modified from this.SetText

using System;
using System.Collections.Generic;
using System.Text;

namespace BackgroundUpdate
{
    internal class ThreadSafe
    {

        //generic system.windows.forms.control
        public static void SetText(System.Windows.Forms.Control ctrl, string text)
        {
            if (ctrl.InvokeRequired)
            {
                object[] params_list = new object[] { ctrl, text };
                ctrl.Invoke(new SetTextDelegate(SetText), params_list);
            }
            else
            {
                ctrl.Text = text;
            }
        }

        //generic system.windows.forms.control
        public static void Dispose(System.Windows.Forms.Control ctrl)
        {
            if (ctrl.InvokeRequired)
            {
                object[] params_list = new object[] { ctrl };
                ctrl.Invoke(new DisposeDelegate(Dispose), params_list);
            }
            else
            {
                ctrl.Dispose();
            }
        }

        //generic system.windows.forms.control
        public static void SetFormText(System.Windows.Forms.Control ctrl, String Text)
        {
            if (ctrl.InvokeRequired)
            {
                object[] params_list = new object[] { ctrl, Text };
                ctrl.Invoke(new SetTextDelegate(SetFormText), params_list);
            }
            else
            {
                ctrl.Text = Text;
            }
        }

        // Modified adding SetProgressBar
        public static void SetProgressBar(System.Windows.Forms.ProgressBar ctrl, float percent)
        {
            if (ctrl.InvokeRequired)
            {
                object[] params_list = new object[] { ctrl, percent };
                ctrl.Invoke(new SetProgressBarDelegate(SetProgressBar), params_list);
            }
            else
            {
                ctrl.Value = (int)percent;
            }
        }

        public static void SetVisible(System.Windows.Forms.Control ctrl, bool visible)
        {
            if (ctrl.InvokeRequired)
            {
                object[] params_list = new object[] { ctrl, visible };
                ctrl.Invoke(new SetVisibleDelegate(SetVisible), params_list);
            }
            else
            {
                ctrl.Visible = visible;
            }
        }

        public static void BringToFront(System.Windows.Forms.Control ctrl)
        {
            if (ctrl.InvokeRequired)
            {
                ctrl.Invoke(new BringToFrontDelegate(BringToFront), ctrl);
            }
            else
            {
                ctrl.BringToFront();
            }
        }
        public static void SendToBack(System.Windows.Forms.Control ctrl)
        {
            if (ctrl.InvokeRequired)
            {
                ctrl.Invoke(new SendToBackDelegate(SendToBack), ctrl);
            }
            else
            {
                ctrl.SendToBack();
            }
        }
        public static void ToggleEnabled(System.Windows.Forms.Control ctrl, bool enabled)
        {
            if (ctrl.InvokeRequired)
            {
                object[] params_list = new object[] { ctrl, enabled };
                ctrl.Invoke(new ToggleEnabledDelegate(ToggleEnabled), params_list);
            }
            else
            {
                ctrl.Enabled = enabled;
            }
        }
        public delegate void SetProgressBarDelegate(System.Windows.Forms.ProgressBar ctrl, float percent);
        public delegate void ToggleEnabledDelegate(System.Windows.Forms.Control ctrl, bool enabled);
        public delegate void SetTextDelegate(System.Windows.Forms.Control ctrl, string text);
        public delegate void SetVisibleDelegate(System.Windows.Forms.Control ctrl, bool visible);
        public delegate void BringToFrontDelegate(System.Windows.Forms.Control ctrl);
        public delegate void SendToBackDelegate(System.Windows.Forms.Control ctrl);
        public delegate void DisposeDelegate(System.Windows.Forms.Control ctrl);

        //checkbox
        public class CheckBox
        {

            public static void Check(System.Windows.Forms.CheckBox ctrl, bool value)
            {
                if (ctrl.InvokeRequired)
                {
                    object[] params_list = new object[] { ctrl, value };
                    ctrl.Invoke(new CheckDelegate(Check), params_list);
                }
                else
                {
                    ctrl.Checked = value;
                }
            }

            public delegate void CheckDelegate(System.Windows.Forms.CheckBox ctrl, bool value);
        }

        //checkbox
        public class RadioButton
        {

            public static void Check(System.Windows.Forms.RadioButton ctrl, bool value)
            {
                if (ctrl.InvokeRequired)
                {
                    object[] params_list = new object[] { ctrl, value };
                    ctrl.Invoke(new CheckDelegate(Check), params_list);
                }
                else
                {
                    ctrl.Checked = value;
                }
            }

            public delegate void CheckDelegate(System.Windows.Forms.RadioButton ctrl, bool value);
        }

        //combobox
        public class ComboBox
        {

            public static void SetSelectedItem(System.Windows.Forms.ComboBox ctrl, object value)
            {
                if (ctrl.InvokeRequired)
                {
                    object[] params_list = new object[] { ctrl, value };
                    ctrl.Invoke(new SetSelectedItemDelegate(SetSelectedItem), params_list);
                }
                else
                {
                    ctrl.SelectedItem = value;
                }
            }

            public static void setSelectedIndex(System.Windows.Forms.ComboBox ctrl, int index)
            {
                if (ctrl.InvokeRequired)
                {
                    object[] params_list = new object[] { ctrl, index };
                    ctrl.Invoke(new setSelectedIndexDelegate(setSelectedIndex), params_list);
                }
                else
                {
                    ctrl.SelectedIndex = index;
                }
            }

            public static void addItem(System.Windows.Forms.ComboBox ctrl, object value)
            {
                if (ctrl.InvokeRequired)
                {
                    object[] params_list = new object[] { ctrl, value };
                    ctrl.Invoke(new addItemDelegate(addItem), params_list);
                }
                else
                {
                    ctrl.Items.Add(value);
                }
            }

            public delegate void setSelectedIndexDelegate(System.Windows.Forms.ComboBox ctrl, int index);
            public delegate void SetSelectedItemDelegate(System.Windows.Forms.ComboBox ctrl, object value);
            public delegate void addItemDelegate(System.Windows.Forms.ComboBox ctrl, object value);

        }

        //datetimepicker
        public class DateTimePicker
        {

            public static void SetDate(System.Windows.Forms.DateTimePicker ctrl, DateTime value)
            {
                if (ctrl.InvokeRequired)
                {
                    object[] params_list = new object[] { ctrl, value };
                    ctrl.Invoke(new SetDateDelegate(SetDate), params_list);
                }
                else
                {
                    ctrl.Value = value;
                }
            }

            public delegate void SetDateDelegate(System.Windows.Forms.DateTimePicker ctrl, DateTime value);

        }


        //listviewitem
        public class ListViewItem
        {

            public static void Check(System.Windows.Forms.ListViewItem ctrl, bool value)
            {
                
                if (ctrl.ListView.InvokeRequired)
                {
                    object[] params_list = new object[] { ctrl, value };
                    ctrl.ListView.Invoke(new CheckDelegate(Check), params_list);
                }
                else
                {
                    ctrl.Checked = value;
                }
                
            }

            public delegate void CheckDelegate(System.Windows.Forms.ListViewItem ctrl, bool value);
        }

        //listview
        public class ListView
        {

            public enum iml_types { small, large };

            public static System.Windows.Forms.ListViewItem getListViewElement(System.Windows.Forms.ListView ctrl, int index)
            {
                
                if (ctrl.InvokeRequired)
                {
                    object[] params_list = new object[] { ctrl, index };
                    System.Windows.Forms.ListViewItem lvi = (System.Windows.Forms.ListViewItem)ctrl.Invoke(new getListViewElementDelegate(getListViewElement), params_list);
                    return lvi;
                    
                }
                else
                {
                    return ctrl.Items[index];
                }

            }
            public static void setImageList(System.Windows.Forms.ListView ctrl, iml_types iml_type, System.Windows.Forms.ImageList iml)
            {

                if (ctrl.InvokeRequired)
                {
                    object[] params_list = new object[] { ctrl, iml_type, iml };
                    ctrl.Invoke(new setImageListDelegate(setImageList), params_list);
                }
                else
                {
                    if (iml_type == iml_types.small)
                    {
                        ctrl.SmallImageList = iml;
                    }
                    else if (iml_type == iml_types.large)
                    {
                        ctrl.LargeImageList = iml;
                    }
                }

            }

            public static void addGroup(System.Windows.Forms.ListView ctrl, System.Windows.Forms.ListViewGroup lvg)
            {

                if (ctrl.InvokeRequired)
                {
                    object[] params_list = new object[] { ctrl, lvg };
                    ctrl.Invoke(new addGroupDelegate(addGroup), params_list);
                }
                else
                {
                    ctrl.Groups.Add(lvg);
                }

            }

            public static void addItem(System.Windows.Forms.ListView ctrl, System.Windows.Forms.ListViewItem lvi)
            {

                if (ctrl.InvokeRequired)
                {
                    object[] params_list = new object[] { ctrl, lvi };
                    ctrl.Invoke(new addListViewItem(addItem), params_list);
                }
                else
                {
                    ctrl.Items.Add(lvi);
                }

            }

            public delegate System.Windows.Forms.ListViewItem getListViewElementDelegate(System.Windows.Forms.ListView ctrl, int index);
            public delegate void setImageListDelegate(System.Windows.Forms.ListView ctrl, iml_types iml_type, System.Windows.Forms.ImageList iml);
            public delegate void addGroupDelegate(System.Windows.Forms.ListView ctrl, System.Windows.Forms.ListViewGroup lvg);
            public delegate void addListViewItem(System.Windows.Forms.ListView ctrl, System.Windows.Forms.ListViewItem lvi);

        }


    }
    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Deployment.Application;
using System.ComponentModel;
using System.Net;
using log4net;

namespace BackgroundUpdate
{
    public class ClickOnceUpdate
    {
        // Static updater
        private static ClickOnceUpdate update = new ClickOnceUpdate();
        // Static timer
        private static Timer timer;

        private static readonly ILog log = LogManager.GetLogger(typeof(ClickOnceUpdate));


        /// <summary>
        /// RestartPending is true when an update has been performed but not applied(it is applied after an app restart)
        /// </summary>
        public static Boolean RestartPending = false;

        /// <summary>
        /// Subscribe to get notifications when an update
        /// </summary>
        public static event AppUpdatedEventHandler AppUpdated;
        public delegate void AppUpdatedEventHandler(Boolean RestartPending);

        /// <summary>
        /// Create the ClickOnceUpdate class. Make constuctor private so that it can't be made twice
        /// </summary>
        private ClickOnceUpdate()
        {
            // Create and subscribe to the timer object
            timer = new System.Timers.Timer();   
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            CheckForUpdate();            
        }
              

        /// <summary>
        /// Start the timer
        /// </summary>
        /// <param name="StartUpdateNow">If true, this method will check immediately (otherwise first update check will be after interval</param>
        /// <param name="interval">Interval of the timer</param>
        public static void StartTimer(Boolean StartUpdateNow, double interval)
        {
            if (StartUpdateNow)
                update.CheckForUpdate();
            timer.Interval = interval;
            ClickOnceUpdate.timer.Start();
        }

        public static void StopTimer()
        {
            timer.Stop();
        }

        // Check for updates
        private void CheckForUpdate()
        {

            if (!HasInternet())
            {
                return;
            }

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                timer.Enabled = false;

                ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;
                ad.CheckForUpdateCompleted += new CheckForUpdateCompletedEventHandler(ad_CheckForUpdateCompleted);

                ad.CheckForUpdateAsync();

            }
        }

        private bool HasInternet()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.wecmk.org"))
                {
                    log.Info("There is Internet Connection");
                    return true;
                }
            }
            catch
            {
                log.Info("no Internet Connection");
                return false;
            }
        }
        

        void ad_CheckForUpdateCompleted(object sender, CheckForUpdateCompletedEventArgs e)
        {
            if (e.UpdateAvailable)
            {
                log.Info("Update availible");
                timer.Enabled = false;
                timer.Stop();
                BeginUpdate();
            }
            else
                timer.Enabled = true;            
        }


        private void BeginUpdate()
        {
            timer.Stop();

            ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;

            ad.UpdateCompleted += new AsyncCompletedEventHandler(ad_UpdateCompleted);

            // incase UpdateAsync is run again (when already updated)
            try
            {
                ad.UpdateAsync();
            }
            catch {}

        }     

        void ad_UpdateCompleted(object sender, AsyncCompletedEventArgs e)
        {
            // Run the event OnHasUpdated
            OnHasUpdate(true);
        }

        protected void OnHasUpdate(Boolean RestartPending)
        {
            if (AppUpdated != null)
            {
                AppUpdated(true);
            }
        }
                   
    }
}

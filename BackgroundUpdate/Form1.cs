﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BackgroundUpdate;
using System.Deployment.Application;
using log4net;

namespace BackgroundUpdate
{
    public partial class Form1 : Form
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Form1));

        public Form1()
        {
            InitializeComponent();
            
            try
            {
                if (Properties.Settings.Default.AppID == "")
                {
                    MessageBox.Show("First Run");
                    Properties.Settings.Default.AppID = Guid.NewGuid().ToString();
                    Properties.Settings.Default.Save();
                    
                }
                label3.Text = "Application Version: " + ApplicationDeployment.CurrentDeployment.CurrentVersion;
                label4.Text = "GUID: " + Properties.Settings.Default.AppID;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }


            // Code to get updating started
            // Code could be placed in Program.cs

            //ClickOnceUpdate.StartTimer(true, 3600000);
            ClickOnceUpdate.StartTimer(true, 10000);
            
            // Subscribe to Update complete (not needed)
            // Can be used to prompt users to restart to get lastest version (or automatically restart)
            ClickOnceUpdate.AppUpdated += new ClickOnceUpdate.AppUpdatedEventHandler(appUpdate_Event);
        }

        private void appUpdate_Event(Boolean RestartRequired)
        {
            // Here we use the event to update the GUI (tell the user the app is updated - and show a button to let them use the most lastest and greatest version)
            pictureBox1.Visible = true;
            label2.Text = "Restart Required? True";
            button1.Visible = true;

            // We could just force the app to restart (although i wouldnt reccomend it)
            // Application.Restart();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        
    }
}
